package business;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.scribe.model.Token;
import org.scribe.model.Verifier;

import model.PhotoStructure;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.photos.GeoData;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.SearchParameters;
import com.flickr4java.flickr.stats.Stats;
import com.flickr4java.flickr.stats.StatsInterface;



public class Controller {
	
	public static final Controller inst = new Controller();
	
	String apiKey;
	String sharedSecret;
	Flickr flickr;
	Auth auth;
	PhotosInterface photosInterface;
	StatsInterface statsInterface;
	List <String> urlList;
	String photoUrl="https://farm5.static.flickr.com/4518/38400837061_9fee6f9c8a_b.jpg";
	List <PhotoStructure> photoScoreList;
	
	
	private Controller() {
		
		/*String apiKey = "4f756471a5a5efa16ca2d39fd2983154";
		String sharedSecret = "25064b12ddaf2bb5";

        flickr = new Flickr(apiKey, sharedSecret, new REST());

		photosInterface = flickr.getPhotosInterface();
		statsInterface = flickr.getStatsInterface();*/
		
		InputStream in = null;
        String apiKey = "4f756471a5a5efa16ca2d39fd2983154";
		String sharedSecret = "25064b12ddaf2bb5";

        flickr = new Flickr(apiKey, sharedSecret, new REST());
        Flickr.debugStream = false;
        AuthInterface authInterface = flickr.getAuthInterface();

        Scanner scanner = new Scanner(System.in);

        Token token = authInterface.getRequestToken();
        System.out.println("token: " + token);

        String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
        System.out.println("Follow this URL to authorise yourself on Flickr");
        System.out.println(url);
        System.out.println("Paste in the token it gives you:");
        System.out.print(">>");

        String tokenKey = scanner.nextLine();
        scanner.close();

        Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
        System.out.println("Authentication success");

        try {
			auth = authInterface.checkToken(requestToken);
		} catch (FlickrException e) {
			e.printStackTrace();
		}
        RequestContext.getRequestContext().setAuth(auth);
		photosInterface = flickr.getPhotosInterface();
		statsInterface = flickr.getStatsInterface();
	}


	public void search(String keyWord) {
		
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setText(keyWord);
		searchParameters.setHasGeo(true);
		PhotoList photoList = new PhotoList();
		photoScoreList=new ArrayList<>();
		urlList=new ArrayList<>();
		try {
			photoList = photosInterface.search(searchParameters, 48, 1);
		} catch (FlickrException e) {
			e.printStackTrace();
		}
		
		for(int i=0;i<photoList.size();i++){
			
			Photo photo = (Photo) photoList.get(i);
			try {
				photo=photosInterface.getInfo(photo.getId(), auth.getTokenSecret());
			} catch (FlickrException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PhotoStructure tmp=new PhotoStructure();
			tmp.setPhoto(photo);
			photoScoreList.add(tmp);
			Stats stats=null;
			try {
				stats = statsInterface.getPhotoStats(photo.getId(), new Date());
			} catch (FlickrException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
			if(stats==null){
				System.out.println("No stats");
			}
			else{
				System.out.println("Views: " + stats.getViews());
				System.out.println("Likes: " + stats.getFavorites());
			}
			
			System.out.println("Date taken: " + photo.getDateTaken());
			
			GeoData geoData = photo.getGeoData();
			if(geoData==null)
				System.out.println("Null");
			else
				System.out.println("Latitude: " + photo.getGeoData().getLatitude() + ", Longitude:" + photo.getGeoData().getLongitude());
			urlList.add(photo.getLargeUrl());
			
		}
//		for (Photo photo : photoList){
//			
//		}
			System.out.println("done");
	}
	
	public void countGpsScore(int latDegree, int latMinute, double latSecond, int longDegree, int longMinute, double longSecond, double weight, String longitudeType, String latitudeType){
		double latitude= (latDegree+latMinute/60.0+latSecond/3600.0);
		double longitude= (longDegree+longMinute/60.0+longSecond/3600.0);
		if(longitudeType.compareTo("West")==0){
			System.out.println("West, nasobim minusem");
			longitude=longitude*-1;
		}
		System.out.println(longitudeType+ " " + latitudeType);
		if(latitudeType.compareTo("South")==0){
			System.out.println("South, nasobim minusem");
			latitude=latitude*-1;
		}
		
		double maxDistance=0;
		
		for(PhotoStructure photoStructure: photoScoreList){
			Photo photo = photoStructure.getPhoto();
			GeoData geodata = photo.getGeoData();
			if(geodata==null){
				photoStructure.setGcd(-1);
				continue;
			}			
			double distance = greatCircleDistance(geodata.getLatitude(),geodata.getLongitude(),latitude,longitude);
			if(distance>maxDistance){
				maxDistance=distance;
			}
			photoStructure.setGcd(distance);
		}
	
		normalizeGpsScore(maxDistance,weight);
	}
	
	private void normalizeGpsScore(double maxDistance, double weight) {
		for(PhotoStructure photoStructure: photoScoreList){
			double distance = photoStructure.getGcd();
			if (distance==-1){
				photoStructure.setGpsScore(0);
				continue;
			}
			photoStructure.setGpsScore((1-distance/maxDistance)*weight);
		}
		
	}
	
	public void countLikesScore(double weight){
		
		int maxLikes=0;
		
		for(PhotoStructure photoStructure: photoScoreList){
			Photo photo = photoStructure.getPhoto();
			Stats stats= photo.getStats();
			if (stats==null){
				continue;
			}
			int likes = photo.getStats().getFavorites();
			if(likes>maxLikes){
				maxLikes=likes;
			}
		}
	
		normalizeLikeScore(maxLikes,weight);
	}
	
	private void normalizeLikeScore(int maxLikes, double weight) {
		for(PhotoStructure photoStructure: photoScoreList){
			Photo photo = photoStructure.getPhoto();
			Stats stats= photo.getStats();
			if (stats==null){
				photoStructure.setLikesScore(0);
				continue;
			}
			int likes = photo.getStats().getFavorites();
			photoStructure.setLikesScore(1.0*likes/maxLikes*weight);
		}		
	}
	
	public void countViewsScore(double weight){
		
		int maxViews=0;
		
		for(PhotoStructure photoStructure: photoScoreList){
			Photo photo = photoStructure.getPhoto();
			Stats stats= photo.getStats();
			if (stats==null){
				continue;
			}
			int views = photo.getStats().getViews();
			if(views>maxViews){
				maxViews=views;
			}
		}
	
		normalizeViewsScore(maxViews,weight);
	}
	
	private void normalizeViewsScore(int maxViews, double weight) {
		for(PhotoStructure photoStructure: photoScoreList){
			Photo photo = photoStructure.getPhoto();
			Stats stats= photo.getStats();
			if (stats==null){
				photoStructure.setViewsScore(0);
				continue;
			}
			int views = photo.getStats().getViews();
			photoStructure.setViewsScore(1.0*views/maxViews*weight);
		}		
	}
	
	public void countDateScore(Date date,double weight){
		
		long time = date.getTime();
		long maxTime=0;
		
		for(PhotoStructure photoStructure: photoScoreList){
			Photo photo = photoStructure.getPhoto();
			Date dateTaken = photo.getDateTaken();
			if (dateTaken==null){
				photoStructure.setTimeDifference(-1);
				continue;
			}
			long photoTimeDiff = dateTaken.getTime()-time;
			if(photoTimeDiff<0){
				photoTimeDiff=photoTimeDiff*-1;
			}
			photoStructure.setTimeDifference(photoTimeDiff);
			if(photoTimeDiff>maxTime){
				maxTime=photoTimeDiff;
			}
		}
	
		normalizeDateScore(maxTime,weight);
	}
	
	private void normalizeDateScore(long maxTime, double weight) {
		for(PhotoStructure photoStructure: photoScoreList){
			long timeDiff=photoStructure.getTimeDifference();
			if (timeDiff==-1){
				photoStructure.setDateScore(0);
			}
			photoStructure.setDateScore((1-1.0*timeDiff/maxTime)*weight);
		}		
	}



	private double greatCircleDistance(double x1, double y1, double x2, double y2){
		double a = Math.pow(Math.sin((x2-x1)/2), 2)
                + Math.cos(x1) * Math.cos(x2) * Math.pow(Math.sin((y2-y1)/2), 2);

       double angle = 2 * Math.asin(Math.min(1, Math.sqrt(a)));

       angle = Math.toDegrees(angle);

       double distance = 60 * angle;
       return distance;
	}
	
	public void rerank(){
		Comparator<PhotoStructure> cmp = new Comparator<PhotoStructure>() {
	        public int compare(PhotoStructure s1, PhotoStructure s2) {
	        	if (s1.getTotalScore()<s2.getTotalScore()) return -1;
	            if (s1.getTotalScore()>s2.getTotalScore()) return 1;
	            return 0;
	        }
	    };
	    System.out.println("Sorting");
	    Collections.sort(photoScoreList, cmp);
	    
	    for(PhotoStructure struct: photoScoreList){
	    	struct.print();
	    }
	}
	
	
	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public List<String> getUrlList() {
		return urlList;
	}

	public void setUrlList(List<String> urlList) {
		this.urlList = urlList;
	}
	
	public List<PhotoStructure> getPhotoScoreList() {
		return photoScoreList;
	}

	public void setPhotoScoreList(List<PhotoStructure> photoScoreList) {
		this.photoScoreList = photoScoreList;
	}


	
}