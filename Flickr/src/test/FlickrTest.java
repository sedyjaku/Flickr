package test;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.photos.GeoData;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoAllContext;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.SearchParameters;
import com.flickr4java.flickr.stats.Stats;
import com.flickr4java.flickr.stats.StatsInterface;
import com.flickr4java.flickr.util.IOUtilities;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Verb;

import model.PhotoStructure;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

import javax.faces.context.FacesContext;


public class FlickrTest {
	
	private static Flickr flickr;
	private static Auth auth;
	private static Token requestToken;
	
	public static void auth() throws IOException, FlickrException {
        InputStream in = null;
        String apiKey = "4f756471a5a5efa16ca2d39fd2983154";
		String sharedSecret = "25064b12ddaf2bb5";

        flickr = new Flickr(apiKey, sharedSecret, new REST());
        Flickr.debugStream = false;
        AuthInterface authInterface = flickr.getAuthInterface();

        Scanner scanner = new Scanner(System.in);

        Token token = authInterface.getRequestToken();
        System.out.println("token: " + token);

        String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
        System.out.println("Follow this URL to authorise yourself on Flickr");
        System.out.println(url);
        System.out.println("Paste in the token it gives you:");
        System.out.print(">>");

        String tokenKey = scanner.nextLine();
        scanner.close();

        requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
//        Auth auth = authInterface.checkToken(requestToken);
        System.out.println("Authentication success");

        auth = authInterface.checkToken(requestToken);

        // This token can be used until the user revokes it.
        System.out.println("Token: " + requestToken.getToken());
        System.out.println("Secret: " + requestToken.getSecret());
        System.out.println("nsid: " + auth.getUser().getId());
        System.out.println("Realname: " + auth.getUser().getRealName());
        System.out.println("Username: " + auth.getUser().getUsername());
        System.out.println("Permission: " + auth.getPermission().getType());
        RequestContext.getRequestContext().setAuth(auth);
        
//        flickr.setAuth(auth);
        
//        authInterface.exchangeAuthToken(requestToken.getSecret());
	}
	
	public static void main(String[] args) {
		
//		test();

		try {
			auth();
			test();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FlickrException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public static void test(){	
		 String apiKey = "4f756471a5a5efa16ca2d39fd2983154";
		String sharedSecret = "25064b12ddaf2bb5";
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		PhotosInterface photosInterface = flickr.getPhotosInterface();
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setHasGeo(true);
		searchParameters.setText("car");
		StatsInterface statsInterface = flickr.getStatsInterface();
		PhotoList photoList = new PhotoList();
		try {
			photoList = photosInterface.search(searchParameters, 10, 1);
			
		} catch (FlickrException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Photo photo = (Photo) photoList.get(0);
		
		for(int i=0;i<photoList.size();i++){
			Photo photo = (Photo) photoList.get(i);
			PhotoStructure tmp=new PhotoStructure();
			tmp.setPhoto(photo);
			System.out.println(photo.getId());
			Stats stats=null;
			try {
//				PhotoAllContext photoAllContext = photosInterface.getAllContexts(photo.getId());
//				photoAllContext.
//				Photo Pho=photosInterface.getInfo(photo.getId(),sharedSecret);
				photo=photosInterface.getInfo(photo.getId(), auth.getTokenSecret());
				System.out.println("Description: " + photo.getDescription());
				stats = statsInterface.getPhotoStats(photo.getId(), new Date());
//				System.out.println("Pho stats:" +Pho.getStats());
//				System.out.println("Perms: " + photosInterface.getPerms(photo.getId()).);
			} catch (FlickrException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(stats==null){
				System.out.println("No stats");
			}
			else{
				System.out.println("Views: " + stats.getViews());
				System.out.println("Likes: " + stats.getFavorites());
			}
			
			System.out.println("Date taken: " + photo.getDateTaken());
			
			GeoData geoData = photo.getGeoData();
			if(geoData==null)
				System.out.println("Null");
			else
				System.out.println("Latitude: " + photo.getGeoData().getLatitude() + ", Longitude:" + photo.getGeoData().getLongitude());
			System.out.println(photo.getUrl());
		}
//		for (Photo photo : photoList){
//			
//		}
			System.out.println("done");
	}
	
	public void login() throws IOException
	{

	AuthInterface authInterface = flickr.getAuthInterface();

    Scanner scanner = new Scanner(System.in);

    Token token = authInterface.getRequestToken();


	}

//	public void test2()
//	{
//		
//		String apiKey = "4f756471a5a5efa16ca2d39fd2983154";
//		String sharedSecret = "25064b12ddaf2bb5";
//		Flickr f = new Flickr(apiKey, sharedSecret, new REST());
//		AuthInterface authInterface = flickr.getAuthInterface();
//	    Token token = authInterface.getRequestToken();
//		OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.flickr.com/services/rest/");
//		request.addQuerystringParameter("method", "flickr.test.login");
//	
//		Token accessToken = authInterface.getAccessToken(token, new Verifier(oauth_verifier));
//		service.signRequest(accessToken, request);
//		Response response = request.send();
//		String body = response.getBody();
//		System.out.println(body);
//	}

}
