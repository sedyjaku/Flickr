package backing;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;

import model.PhotoStructure;
import business.Controller;

@ManagedBean
public class SearchBean {

private String keyWord;
private boolean gpsFlag;
private boolean dateFlag;
private boolean likeFlag;
private boolean viewsFlag;
private int gpsWeight;
private int dateWeight;
private int likesWeight;
private int viewsWeight;
private String longitudeSelect;
private String latitudeSelect;
private int gpsLatDegree;
private int gpsLatMin;
private double gpsLatSec;
private int gpsLongDegree;
private int gpsLongMin;
private double gpsLongSec;
private Date date;



	public String search()
	{
		Controller.inst.search(keyWord);
		return "index?faces-redirect=true";
	}
	
	public String rerank(){
		Controller controller=Controller.inst;
		System.out.println("Gps weight:"  + gpsWeight + " , Date Weight:" + dateWeight + ", likesWeight: " + likesWeight + ", viewsWeight: " + viewsWeight);
		System.out.println("GPS longitude: " + gpsLatDegree + " ," + gpsLatMin + " ," + gpsLatSec );
		System.out.println("GPS lattitude: " + gpsLongDegree + " ," + gpsLongMin + " ," + gpsLongSec );
		System.out.println("Date:" + date);
		System.out.println("Reranking");
		if(likeFlag){
			System.out.println("likes");
			controller.countViewsScore(likesWeight);
		}
		if(viewsFlag){
			System.out.println("view");
			controller.countLikesScore(likesWeight);
		}
		if(gpsFlag){
			System.out.println("gps");
			controller.countGpsScore(gpsLatDegree, gpsLatMin, gpsLatSec, gpsLongDegree,gpsLongMin, gpsLongSec, gpsWeight, longitudeSelect, latitudeSelect);
		}
		if (dateFlag){
			System.out.println("date");
			controller.countDateScore(date, dateWeight);
		}
		controller.rerank();
		return "index?faces-redirect=true";
	}


	public String getKeyWord() {
		return keyWord;
	}


	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	
	public List<String> getUrlList(){
		return Controller.inst.getUrlList();
	}
	
	public List<PhotoStructure> getPhotoList(){
		return Controller.inst.getPhotoScoreList();
	}

	public int getGpsWeight() {
		return gpsWeight;
	}


	public void setGpsWeight(int gpsWeight) {
		this.gpsWeight = gpsWeight;
	}


	public int getDateWeight() {
		return dateWeight;
	}


	public void setDateWeight(int dateWeight) {
		this.dateWeight = dateWeight;
	}
	
	public int getLikesWeight() {
		return likesWeight;
	}


	public void setLikesWeight(int likesWeight) {
		this.likesWeight = likesWeight;
	}


	public int getViewsWeight() {
		return viewsWeight;
	}


	public void setViewsWeight(int viewsWeight) {
		this.viewsWeight = viewsWeight;
	}


	public boolean isGpsFlag() {
		return gpsFlag;
	}


	public void setGpsFlag(boolean gpsFlag) {
		this.gpsFlag = gpsFlag;
	}


	public boolean isDateFlag() {
		return dateFlag;
	}


	public void setDateFlag(boolean dateFlag) {
		this.dateFlag = dateFlag;
	}
	
	public boolean isLikeFlag() {
		return likeFlag;
	}


	public void setLikeFlag(boolean likeFlag) {
		this.likeFlag = likeFlag;
	}


	public boolean isViewsFlag() {
		return viewsFlag;
	}


	public void setViewsFlag(boolean viewsFlag) {
		this.viewsFlag = viewsFlag;
	}
	
	public String getLongitudeSelect() {
		return longitudeSelect;
	}

	public void setLongitudeSelect(String longitudeSelect) {
		this.longitudeSelect = longitudeSelect;
	}

	public String getLatitudeSelect() {
		return latitudeSelect;
	}


	public void setLatitudeSelect(String latitudeSelect) {
		this.latitudeSelect = latitudeSelect;
	}


	public int getGpsLatDegree() {
		return gpsLatDegree;
	}


	public void setGpsLatDegree(int gpsLatDegree) {
		this.gpsLatDegree = gpsLatDegree;
	}


	public int getGpsLatMin() {
		return gpsLatMin;
	}


	public void setGpsLatMin(int gpsLatMin) {
		this.gpsLatMin = gpsLatMin;
	}


	public double getGpsLatSec() {
		return gpsLatSec;
	}


	public void setGpsLatSec(double gpsLatSec) {
		this.gpsLatSec = gpsLatSec;
	}


	public int getGpsLongDegree() {
		return gpsLongDegree;
	}


	public void setGpsLongDegree(int gpsLongDegree) {
		this.gpsLongDegree = gpsLongDegree;
	}


	public int getGpsLongMin() {
		return gpsLongMin;
	}


	public void setGpsLongMin(int gpsLongMin) {
		this.gpsLongMin = gpsLongMin;
	}


	public double getGpsLongSec() {
		return gpsLongSec;
	}


	public void setGpsLongSec(double gpsLongSec) {
		this.gpsLongSec = gpsLongSec;
	}



	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	
	
}
